# The Official Goldenrod Book (DRAFT)

<div align="center">

```
╭───────────────────╮
│     ___/__%%%%%%  │
│   -¯    \         │
│  /                │
╰───────────────────╯
```

</div>

Welcome to Goldenrod, an opinionated, lightweight, and performant GUI library geared towards audio software. Goldenrod is written in Rust, and official bindings to C and C++ are provided (*soon*).

## Who Goldenrod is For

Unlike other GUI libraries found in the Rust ecosystem (and any other modern GUI library for that matter), this library is *NOT* declarative, reactive, immediate-mode, or data-driven. Instead it uses a simple but novel approach which prioritizes performance and manual control. Goldenrod won't get in your way, but it won't hold your hand either.

As such, Goldenrod is not for everyone. Any other GUI library which provides a modern API that is declarative, reactive, immediate-mode, and/or data-driven will probably appeal to more people and provide a smoother development experience. See *TODO* for a list of other recommended libraries to try.

The aim is not to be "general purpose". Some features are intentionally omitted for the sake of simplicity and performance. The main target is audio software, and so that use case takes priority.

> This project came about as a need for a high-performance and flexible GUI library for my Meadowlark DAW and related audio projects (in addition to needing some niche OS-level features that are not usually found in general-purpose GUI libraries). After trying many different libraries, it became apparent that creating an in-house solution would be better in the long term over trying to hack, limit, and upkeep my way around some other library. I also have my own opinions on how I want a GUI library to work, and so I decided to pursue Goldenrod.

## Foreward

In this book we will work through how to write a simple application, and highlight both the benefits and some notable intentional limitations of this library.

[>> Next: Chapter 1 - Installation and Setup](CHAPTER_1.md)