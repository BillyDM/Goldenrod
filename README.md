<div align="center">

# Goldenrod

```
╭───────────────────╮
│     ___/__%%%%%%  │
│   -¯    \         │
│  /                │
╰───────────────────╯
```

**An opinionated, lightweight, and performant GUI library geared towards audio software**

[![License](https://img.shields.io/crates/l/goldenrod.svg)](https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE)

</div>

> **Work in progress. This library is not ready for any kind of use.**

# About

Goldenrod is a simple, "low level" GUI framework aimed mainly towards audio software and audio plugins.

Unlike other GUI libraries found in the Rust ecosystem (and any other modern GUI library for that matter), this library is *NOT* declarative, reactive, immediate-mode, or data-driven. Instead it uses a simple but novel approach which prioritizes performance and manual control. Goldenrod won't get in your way, but it won't hold your hand either.

As such, Goldenrod is not for everyone. Any other GUI library which provides a modern API that is declarative, reactive, immediate-mode, and/or data-driven will probably appeal to more people and provide a smoother development experience. See *TODO* for a list of other recommended libraries to try.

The aim is not to be "general purpose". Some features are intentionally omitted for the sake of simplicity and performance. The main target is audio software, and so that use case takes priority.

# Features

* Cross-platform (Linux, Mac, and Windows)
* Native, extremely lightweight and performant
* Can be used for both standalone applications and audio plugins
* Elements can be drawn using textures, vector graphics, and/or custom shaders.
* Designed from the ground-up to support multi-windowed applications
* Bindings for C and C++. Goldenrod does not depend on any Rust type system wizardry for its API, so the bindings don't have any compromises compared to the native Rust API (aside from compiler-guaranteed memory safety of course).
* Accessibility support* *(see section below)*
* [Permissive MIT license](./LICENSE)

# Non-features

* Many aspects of the layout and styling process which are commonplace in other GUI libraries function quite differently in Goldenrod. See the [book] for more details.
* While there is accessibility support, Goldenrod does not automatically set this up for you. You must manually provide Goldenrod with information on how accessibility tools should navigate your program.
* The official backends only targets Linux, Mac, and Windows. Support for other platforms are not currently planned.

# Get Started

To get started, read the [book] (WIP).

> This repository only houses the GUI library. For examples and guides on how to use Goldenrod for audio plugin development, see *TODO*.

# Contributing

*TODO*

[book]: ./docs/book_draft/INTRO.md
[JUCE]: https://juce.com/