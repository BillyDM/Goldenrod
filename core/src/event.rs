// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

use keyboard_types::{CompositionEvent, KeyboardEvent, Modifiers};
use std::any::Any;

use crate::geometry::LogicalPoint;

pub enum CanvasEvent {
    Animation { delta_seconds: f64 },
    Pointer(PointerEvent),
    Keyboard(KeyboardEvent),
    TextComposition(CompositionEvent),
    WindowHidden,
    WindowShown,
    WindowFocused,
    WindowUnfocused,
}

pub enum Event {
    Custom(Box<dyn Any>),
    Animation { delta_seconds: f64 },
    Hidden,
    Shown,
    Pointer(PointerEvent),
    Keyboard(KeyboardEvent),
    TextComposition(CompositionEvent),
    SizeChanged,
    ZIndexChanged,
    GotExclusiveFocus,
    ExclusiveFocusReleased,
    Init,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PointerButtonState {
    StayedUnpressed,
    StayedPressed,
    JustPressed,
    JustUnpressed,
}

impl PointerButtonState {
    pub fn just_pressed(&self) -> bool {
        *self == PointerButtonState::JustPressed
    }

    pub fn just_unpressed(&self) -> bool {
        *self == PointerButtonState::JustUnpressed
    }

    pub fn is_down(&self) -> bool {
        *self == PointerButtonState::JustPressed || *self == PointerButtonState::StayedPressed
    }
}

impl Default for PointerButtonState {
    fn default() -> Self {
        PointerButtonState::StayedUnpressed
    }
}

#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub enum PointerType {
    Mouse,
    Pen,
    Touch,
    #[default]
    Unknown,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum WheelDeltaType {
    Pixels,
    Lines,
    Pages,
}

#[derive(Debug, Clone, PartialEq)]
pub struct PointerEvent {
    pub position: LogicalPoint,
    pub delta: LogicalPoint,
    /// Whether or not the backend has locked the pointer in place.
    ///
    /// This will only be `true` if all the following conditions are true:
    /// * This element has requested to steal focus and lock the pointer.
    /// * This element has exclusive focus.
    /// * The backend supports locking the pointer.
    ///
    /// Note if this is `false`, then you will generally want to use
    /// `position` instead of `delta` for better accuracy.
    pub is_locked: bool,
    pub pointer_type: PointerType,

    pub primary_button: PointerButtonState,
    pub secondary_button: PointerButtonState,
    pub auxiliary_button: PointerButtonState,
    pub fourth_button: PointerButtonState,
    pub fifth_button: PointerButtonState,

    pub wheel_delta_type: WheelDeltaType,
    pub wheel_delta_x: f32,
    pub wheel_delta_y: f32,

    pub modifiers: Modifiers,
}

impl PointerEvent {
    /*
    #[cfg(feature = "winit")]
    pub fn update_from_winit_cursor_moved_event(
        &mut self,
        position: winit::dpi::PhysicalPosition<f64>,
        scale_factor: ScaleFactor,
    ) {
        self.scroll_delta_x = 0.0;
        self.scroll_delta_y = 0.0;

        let new_pos = Point::new(
            position.x / scale_factor.as_f64(),
            position.y / scale_factor.as_f64(),
        );

        self.delta = new_pos - self.position;
        self.position = new_pos;
    }

    #[cfg(feature = "winit")]
    pub fn update_from_winit_mouse_input_event(
        &mut self,
        state: winit::event::ElementState,
        button: winit::event::MouseButton,
    ) {
        self.scroll_delta_x = 0.0;
        self.scroll_delta_y = 0.0;

        let is_down = state == winit::event::ElementState::Pressed;

        let handle_button = |button_state: &mut PointerButtonState| match button_state {
            PointerButtonState::StayedUnpressed => {
                if is_down {
                    *button_state = PointerButtonState::JustPressed;
                }
            }
            PointerButtonState::StayedPressed => {
                if !is_down {
                    *button_state = PointerButtonState::JustUnpressed;
                }
            }
            PointerButtonState::JustPressed => {
                if is_down {
                    *button_state = PointerButtonState::StayedPressed;
                } else {
                    *button_state = PointerButtonState::JustUnpressed;
                }
            }
            PointerButtonState::JustUnpressed => {
                if is_down {
                    *button_state = PointerButtonState::JustPressed;
                } else {
                    *button_state = PointerButtonState::StayedUnpressed;
                }
            }
        };

        match button {
            winit::event::MouseButton::Left => handle_button(&mut self.left_button),
            winit::event::MouseButton::Middle => handle_button(&mut self.middle_button),
            winit::event::MouseButton::Right => handle_button(&mut self.right_button),
            _ => (),
        }
    }

    #[cfg(feature = "winit")]
    pub fn update_from_winit_mouse_wheel_event(
        &mut self,
        delta: winit::event::MouseScrollDelta,
        _phase: winit::event::TouchPhase,
        scale_factor: ScaleFactor,
    ) {
        const PIXELS_PER_LINE: f32 = 12.0;

        self.scroll_delta_x = 0.0;
        self.scroll_delta_y = 0.0;

        match delta {
            winit::event::MouseScrollDelta::LineDelta(x, y) => {
                self.scroll_delta_x = x * PIXELS_PER_LINE / scale_factor.as_f32();
                self.scroll_delta_y = y * PIXELS_PER_LINE / scale_factor.as_f32();
            }
            winit::event::MouseScrollDelta::PixelDelta(delta) => {
                self.scroll_delta_x = delta.x as f32 / scale_factor.as_f32();
                self.scroll_delta_y = delta.y as f32 / scale_factor.as_f32();
            }
        }
    }
    */
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EventCaptureStatus {
    NotCaptured,
    Captured,
}
