// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

mod context;
mod flags;
mod handle;

pub use context::ElementContext;
pub use flags::ElementFlags;
pub use handle::ElementHandle;

use super::{ClipAreaID, ElementID, LayerID, CANVAS_CLIP_AREA_ID};
use crate::action_queue::ActionSender;
use crate::event::{Event, EventCaptureStatus};
use crate::geometry::{LogicalRect, ZIndex};
use crate::stmpsc_queue;

pub trait Element {
    fn flags(&self) -> ElementFlags;

    fn on_event(
        &mut self,
        event: Event,
        cx: &mut ElementContext,
        action_sender: &mut ActionSender,
    ) -> EventCaptureStatus;

    #[allow(unused)]
    fn on_dropped(&mut self, action_sender: &mut ActionSender) {}
}

pub struct ElementBuilder {
    pub element: Box<dyn Element>,
    pub layer_id: LayerID,
    pub z_index: ZIndex,
    pub rect: LogicalRect,
    pub manually_hidden: bool,
    pub clip_area_id: ClipAreaID,
}

impl ElementBuilder {
    pub fn new(element: Box<dyn Element>) -> Self {
        Self {
            element,
            layer_id: LayerID::Main,
            z_index: 0,
            rect: LogicalRect::default(),
            manually_hidden: false,
            clip_area_id: CANVAS_CLIP_AREA_ID,
        }
    }

    pub const fn layer(mut self, layer_id: LayerID) -> Self {
        self.layer_id = layer_id;
        self
    }

    pub const fn z_index(mut self, z_index: ZIndex) -> Self {
        self.z_index = z_index;
        self
    }

    pub const fn rect(mut self, rect: LogicalRect) -> Self {
        self.rect = rect;
        self
    }

    pub const fn hidden(mut self, hidden: bool) -> Self {
        self.manually_hidden = hidden;
        self
    }

    pub const fn clip_area(mut self, clip_area_id: ClipAreaID) -> Self {
        self.clip_area_id = clip_area_id;
        self
    }
}

pub(super) struct ElementModification {
    pub element_id: ElementID,
    pub type_: ElementModificationType,
}

pub(super) enum ElementModificationType {
    MarkDirty,
    RectChanged(LogicalRect),
    ClipAreaChanged,
    ZIndexChanged(ZIndex),
    ExplicitlyHiddenChanged(bool),
    SetAnimating(bool),
    StealExclusiveFocus,
    ReleaseExclusiveFocus,
    HandleDropped,
}

// I get a warning about leaking `ElementID` if I make `ElementHandle::new()`
// have `public(crate)` visibility, so this is a workaround.
pub(super) fn new_element_handle(
    element_id: ElementID,
    mod_queue_sender: stmpsc_queue::Sender<ElementModification>,
    rect: LogicalRect,
    z_index: ZIndex,
    manually_hidden: bool,
) -> ElementHandle {
    ElementHandle::new(element_id, mod_queue_sender, rect, z_index, manually_hidden)
}
