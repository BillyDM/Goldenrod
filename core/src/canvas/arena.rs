// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

#[derive(Debug, Clone, Copy)]
pub(crate) struct ArenaID {
    index: usize,
    id: u64,
}

impl PartialEq for ArenaID {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for ArenaID {}

struct ArenaEntry<S: Clone, B> {
    stack_data: S,
    // The part of the data that is on the heap (in our case `Box<dyn Element>`)
    // is separated here so the compiler can optimize this `Option` to use
    // null pointer optimizations.
    boxed_data: Option<B>,
    id: u64,
}

/// A simple index-based arena.
///
/// My original plan was to use Rc/Weak pointers to avoid the overhead of array
/// indexing. But in addition to that being a lot more cumbersome to write, I
/// found that using an arena helps ensure that all elements are closer together
/// in memory, and the better cache locality helps make up for the overhead. So
/// the simpler solution wins.
pub(super) struct IdArena<S: Clone, B> {
    slots: Vec<ArenaEntry<S, B>>,
    free_slots: Vec<usize>,
    next_id: u64,
}

impl<S: Clone, B> IdArena<S, B> {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            slots: Vec::with_capacity(capacity),
            free_slots: Vec::with_capacity(capacity),
            next_id: 1, // id 0 is reserved for "empty slot"
        }
    }

    pub fn insert(&mut self, stack_data: S, boxed_data: B) -> ArenaID {
        let id = self.next_id;
        self.next_id += 1;

        let index = if let Some(index) = self.free_slots.pop() {
            self.slots[index] = ArenaEntry {
                stack_data,
                boxed_data: Some(boxed_data),
                id,
            };

            index
        } else {
            let index = self.free_slots.len();

            self.slots.push(ArenaEntry {
                stack_data,
                boxed_data: Some(boxed_data),
                id,
            });

            index
        };

        ArenaID { index, id }
    }

    pub fn remove(&mut self, id: ArenaID) -> Option<(S, B)> {
        if let Some(slot) = self.slots.get_mut(id.index) {
            if slot.id == id.id {
                slot.id = 0; // id 0 is reserved for "empty slot"

                self.free_slots.push(id.index);

                if let Some(boxed_data) = slot.boxed_data.take() {
                    return Some((slot.stack_data.clone(), boxed_data));
                }
            }
        }

        None
    }

    pub fn get_stack_data(&self, id: ArenaID) -> Option<&S> {
        // TODO: In theory we can use unsafe to bypass this bounds check,
        // as long as we make sure that the caller passes in an `ArenaID`
        // instance that was created by this `IdArena`, and not some other
        // `IdArena` (which the caller should already be doing anyway).
        //
        // However, this is probably a premature optimization and it would
        // pollute the parent scope with unasfe, so for now I'm leaving
        // this as is.
        if let Some(slot) = self.slots.get(id.index) {
            if slot.id == id.id {
                return Some(&slot.stack_data);
            }
        };

        None
    }

    pub fn get_stack_data_mut(&mut self, id: ArenaID) -> Option<&mut S> {
        // TODO: In theory we can use unsafe to bypass this bounds check,
        // as long as we make sure that the caller passes in an `ArenaID`
        // instance that was created by this `IdArena`, and not some other
        // `IdArena` (which the caller should already be doing anyway).
        //
        // However, this is probably a premature optimization and it would
        // pollute the parent scope with unasfe, so for now I'm leaving
        // this as is.
        if let Some(slot) = self.slots.get_mut(id.index) {
            if slot.id == id.id {
                return Some(&mut slot.stack_data);
            }
        };

        None
    }

    pub fn get_mut(&mut self, id: ArenaID) -> Option<(&mut S, &mut B)> {
        // TODO: In theory we can use unsafe to bypass this bounds check,
        // as long as we make sure that the caller passes in an `ArenaID`
        // instance that was created by this `IdArena`, and not some other
        // `IdArena` (which the caller should already be doing anyway).
        //
        // However, this is probably a premature optimization and it would
        // pollute the parent scope with unasfe, so for now I'm leaving
        // this as is.
        if let Some(slot) = self.slots.get_mut(id.index) {
            if slot.id == id.id {
                // SAFETY:
                // This can only be `None` if the slot is empty. Since an
                // `ArenaID` cannot contain the ID of 0 (the ID reserved
                // for an empty slot), and since we checked that the ID of
                // this slot matches the ID of an `ArenaID`, we know that
                // this slot cannot be empty.
                let boxed_data = unsafe { slot.boxed_data.as_mut().unwrap_unchecked() };

                return Some((&mut slot.stack_data, boxed_data));
            }
        }

        None
    }
}
