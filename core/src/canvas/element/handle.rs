// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

use super::ElementModificationType;
use crate::canvas::{ElementID, ElementModification};
use crate::geometry::{LogicalPoint, LogicalRect, LogicalSize, ZIndex};
use crate::stmpsc_queue;

pub struct ElementHandle {
    element_id: ElementID,
    mod_queue_sender: stmpsc_queue::Sender<ElementModification>,

    rect: LogicalRect,
    z_index: ZIndex,
    manually_hidden: bool,
}

impl ElementHandle {
    pub(super) fn new(
        element_id: ElementID,
        mod_queue_sender: stmpsc_queue::Sender<ElementModification>,
        rect: LogicalRect,
        z_index: ZIndex,
        manually_hidden: bool,
    ) -> Self {
        Self {
            element_id,
            mod_queue_sender,
            rect,
            z_index,
            manually_hidden,
        }
    }

    /// Get the rectangular area assigned to this element instance.
    pub fn rect(&self) -> &LogicalRect {
        &self.rect
    }

    /// Get the z index of this element instance.
    pub fn z_index(&self) -> ZIndex {
        self.z_index
    }

    /// Returns `true` if the element instance has been manually hidden.
    ///
    /// Note that even if this returns `true`, the element may still be hidden
    /// due to it being outside of the render area.
    pub fn manually_hidden(&self) -> bool {
        self.manually_hidden
    }

    /// Set the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    pub fn set_rect(&mut self, rect: LogicalRect) {
        if self.rect != rect {
            self.rect = rect;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(rect),
            });
        }
    }

    /// Set the position of the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    ///
    /// Note, it is more efficient to use `ElementHandle::set_rect()` than
    /// to set the position and size separately.
    pub fn set_pos(&mut self, pos: LogicalPoint) {
        if self.rect.pos != pos {
            self.rect.pos = pos;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(self.rect),
            });
        }
    }

    /// Set the size of the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    ///
    /// Note, it is more efficient to use `ElementHandle::set_rect()` than
    /// to set the position and size separately.
    pub fn set_size(&mut self, size: LogicalSize) {
        if self.rect.size != size {
            self.rect.size = size;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(self.rect),
            });
        }
    }

    /// Set the x position of the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    ///
    /// Note, it is more efficient to use `ElementHandle::set_pos()` or
    /// `ElementHandle::set_rect()` than to set the fields of the rectangle
    /// separately.
    pub fn set_x(&mut self, logical_x: f32) {
        if self.rect.pos.x != logical_x {
            self.rect.pos.x = logical_x;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(self.rect),
            });
        }
    }

    /// Set the y position of the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    ///
    /// Note, it is more efficient to use `ElementHandle::set_pos()` or
    /// `ElementHandle::set_rect()` than to set the fields of the rectangle
    /// separately.
    pub fn set_y(&mut self, logical_y: f32) {
        if self.rect.pos.y != logical_y {
            self.rect.pos.y = logical_y;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(self.rect),
            });
        }
    }

    /// Set the width of the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    ///
    /// Note, it is more efficient to use `ElementHandle::set_size()` or
    /// `ElementHandle::set_rect()` than to set the fields of the rectangle
    /// separately.
    pub fn set_width(&mut self, logical_width: f32) {
        if self.rect.size.width() != logical_width {
            self.rect.size.set_width(logical_width);
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(self.rect),
            });
        }
    }

    /// Set the height of the rectangular area of this element instance.
    ///
    /// An update will only be sent to the canvas if the rectangle has changed.
    ///
    /// Note, it is more efficient to use `ElementHandle::set_size()` or
    /// `ElementHandle::set_rect()` than to set the fields of the rectangle
    /// separately.
    pub fn set_height(&mut self, logical_height: f32) {
        if self.rect.size.height() != logical_height {
            self.rect.size.set_height(logical_height);
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::RectChanged(self.rect),
            });
        }
    }

    /// Set the z index of this element instance.
    ///
    /// An update will only be sent to the canvas if the z index has changed.
    pub fn set_z_index(&mut self, z_index: ZIndex) {
        if self.z_index != z_index {
            self.z_index = z_index;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::ZIndexChanged(z_index),
            });
        }
    }

    /// Set to hide or show this element instance.
    ///
    /// Note, there is no need to hide elements just because they appear outside
    /// of the render area. The canvas already handles that for you.
    ///
    /// An update will only be sent to the canvas if the visibility request
    /// has changed since the previous call.
    pub fn set_hidden(&mut self, hidden: bool) {
        if self.manually_hidden != hidden {
            self.manually_hidden = hidden;
            self.mod_queue_sender.send(ElementModification {
                element_id: self.element_id,
                type_: ElementModificationType::ExplicitlyHiddenChanged(hidden),
            });
        }
    }
}

impl Drop for ElementHandle {
    fn drop(&mut self) {
        self.mod_queue_sender.send(ElementModification {
            element_id: self.element_id,
            type_: ElementModificationType::HandleDropped,
        });
    }
}
