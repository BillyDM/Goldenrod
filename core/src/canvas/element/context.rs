// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

use crate::geometry::{LogicalRect, ZIndex};

/// A context for this element instance. This is used to request actions from the
/// UI library.
pub struct ElementContext {
    rect: LogicalRect,
    z_index: ZIndex,
    manually_hidden: bool,
    animating: bool,
    repaint_requested: bool,
    visible: bool,
    has_exclusive_focus: bool,
}

impl ElementContext {
    pub(crate) fn new(
        rect: LogicalRect,
        z_index: ZIndex,
        manually_hidden: bool,
        animating: bool,
        visible: bool,
        has_exclusive_focus: bool,
    ) -> Self {
        Self {
            rect,
            z_index,
            manually_hidden,
            animating,
            repaint_requested: false,
            visible,
            has_exclusive_focus,
        }
    }

    /// The rectangular area assigned to this element instance.
    ///
    /// Note, the rectangle may have a position and size of zero if the element
    /// has yet to be laid out.
    pub fn rect(&self) -> &LogicalRect {
        &self.rect
    }

    /// The z index of this element instance.
    pub fn z_index(&self) -> ZIndex {
        self.z_index
    }

    /// Whether or not the user manually set this element instance to be hidden
    /// via this element's handle.
    ///
    /// Note this differs from `ElementContext::is_visible()` in that this element
    /// may still be invisible due to it being outside of the render area.
    pub fn manually_hidden(&self) -> bool {
        self.manually_hidden
    }

    /// Whether or not this element instance is currently receiving the animation
    /// event.
    pub fn is_animating(&self) -> bool {
        self.animating
    }

    /// Returns true if the element is currently visible on the screen.
    ///
    /// This will return `false` if one or more of these scenarios is true:
    /// * This user or the element has manually set this to be hidden.
    /// * This element lies outside of the render area.
    pub fn is_visible(&self) -> bool {
        self.visible
    }

    /// Returns `true` if this element currenly has exclusive focus, `false`
    /// otherwise.
    pub fn has_exclusive_focus(&self) -> bool {
        self.has_exclusive_focus
    }

    /// Request to repaint this element this frame.
    ///
    /// This will also cause all child elements to be repainted.
    pub fn request_repaint(&mut self) {
        self.repaint_requested = true;
    }

    /// Set/unset whether this element should receive the animation event. The
    /// animation event is sent every frame just before rendering begins.
    ///
    /// Once the element instance is done animating, prefer to unset this to save on
    /// system resources.
    ///
    /// By default every newly created element instance does not listen to this
    /// event.
    pub fn set_animating(&mut self, animating: bool) {
        self.animating = animating;
    }

    /// Request to steal exclusive focus.
    ///
    /// If another element instance has exclusive focus, then that element will
    /// automatically be unfocused.
    ///
    /// By default every newly created element does not have exclusive focus.
    pub fn steal_exclusive_focus(&mut self) {
        self.has_exclusive_focus = true;
    }

    /// Request to release exclusive focus.
    pub fn release_exclusive_focus(&mut self) {
        self.has_exclusive_focus = false;
    }

    pub(crate) fn repaint_requested(&self) -> bool {
        self.repaint_requested
    }
}
