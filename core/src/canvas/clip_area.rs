// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

use super::{arena::IdArena, ElementEntry, ElementID};
use crate::element::{Element, ElementModification, ElementModificationType};
use crate::geometry::{LogicalPoint, LogicalRect};
use crate::stmpsc_queue;

/// `ClipAreaID` of `0` means to use use the canvas itself as the
/// clip area.
pub const CANVAS_CLIP_AREA_ID: ClipAreaID = 0;

pub type ClipAreaID = u8;

#[derive(Debug, Clone, PartialEq)]
pub struct ClipAreaConfig {
    pub rect: LogicalRect,
    pub scroll_offset: LogicalPoint,
}

pub(super) struct ClipArea {
    rect: LogicalRect,
    scroll_offset: LogicalPoint,
    assigned_elements: Vec<ElementID>,
}

impl ClipArea {
    pub fn new(config: ClipAreaConfig) -> Self {
        let ClipAreaConfig {
            rect,
            scroll_offset,
        } = config;

        assert!(rect.width() >= 1.0);
        assert!(rect.height() >= 1.0);

        Self {
            rect,
            scroll_offset,
            assigned_elements: Vec::new(),
        }
    }

    pub fn rect(&self) -> &LogicalRect {
        &self.rect
    }

    /// Returns `true` if the rect changed, `false` otherwise.
    pub fn set_rect(
        &mut self,
        rect: LogicalRect,
        mod_queue_sender: &mut stmpsc_queue::Sender<ElementModification>,
    ) -> bool {
        if self.rect == rect {
            return false;
        }

        assert!(rect.width() >= 1.0);
        assert!(rect.height() >= 1.0);

        self.rect = rect;

        for element_id in self.assigned_elements.iter() {
            mod_queue_sender.send_to_front(ElementModification {
                element_id: *element_id,
                type_: ElementModificationType::ClipAreaChanged,
            });
        }

        true
    }

    pub fn pos(&self) -> LogicalPoint {
        self.rect.pos
    }

    pub fn scroll_offset(&self) -> LogicalPoint {
        self.scroll_offset
    }

    pub fn set_scroll_offset(
        &mut self,
        scroll_offset: LogicalPoint,
        mod_queue_sender: &mut stmpsc_queue::Sender<ElementModification>,
    ) {
        if self.scroll_offset == scroll_offset {
            return;
        }

        self.scroll_offset = scroll_offset;

        for element_id in self.assigned_elements.iter() {
            mod_queue_sender.send_to_front(ElementModification {
                element_id: *element_id,
                type_: ElementModificationType::ClipAreaChanged,
            });
        }
    }

    pub fn add_element(&mut self, element_entry: &mut ElementEntry, element_id: ElementID) {
        element_entry.index_in_clip_area_list = self.assigned_elements.len() as u32;

        self.assigned_elements.push(element_id);
    }

    pub fn remove_element(
        &mut self,
        element_entry: &ElementEntry,
        element_arena: &mut IdArena<ElementEntry, Box<dyn Element>>,
    ) {
        let _ = self
            .assigned_elements
            .swap_remove(element_entry.index_in_clip_area_list as usize);

        // Update the index in the element that was swapped.
        if let Some(swapped_element_id) = self
            .assigned_elements
            .get(element_entry.index_in_clip_area_list as usize)
        {
            element_arena
                .get_stack_data_mut(swapped_element_id.0)
                .unwrap()
                .index_in_clip_area_list = element_entry.index_in_clip_area_list;
        }
    }
}
