// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

mod arena;
mod clip_area;
pub mod element;
mod layer;

use keyboard_types::CompositionEvent;
use keyboard_types::KeyboardEvent;

pub use clip_area::{ClipAreaConfig, ClipAreaID, CANVAS_CLIP_AREA_ID};

use self::arena::{ArenaID, IdArena};
use self::clip_area::ClipArea;
use self::element::{
    Element, ElementBuilder, ElementContext, ElementFlags, ElementHandle, ElementModification,
    ElementModificationType,
};
use self::layer::{ElementLayerData, Layer};
use crate::action_queue::ActionSender;
use crate::event::CanvasEvent;
use crate::event::{Event, EventCaptureStatus, PointerEvent};
use crate::geometry::{LogicalPoint, LogicalRect, LogicalSize, ZIndex};
use crate::stmpsc_queue;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ElementID(ArenaID);

/// The settings for a new `Canvas`.
#[derive(Debug, Clone, PartialEq)]
pub struct CanvasConfig {
    /// The initial size of the canvas.
    ///
    /// By default this is set to a width of `480` and a height of `240`.
    pub canvas_size: LogicalSize,

    /// Any additional clipping areas to add.
    ///
    /// New clipping areas cannot be added later, nor can clipping areas be
    /// removed.
    ///
    /// By default this is empty (no additional clipping areas).
    pub additional_clip_areas: Vec<ClipAreaConfig>,

    /// The total number of different z indexes that can be in the background layer
    /// (including the `0`th z index).
    ///
    /// Additional z indexes cannot be added later.
    ///
    /// By default this is set to `4`.
    pub num_z_indexes_in_background_layer: u8,
    /// The total number of different z indexes that can be in the main layer
    /// (including the `0`th z index).
    ///
    /// By default this is set to `4`.
    pub num_z_indexes_in_main_layer: u8,
    /// The total number of different z indexes that can be in the overlay layer
    /// (including the `0`th z index).
    ///
    /// By default this is set to `4`.
    pub num_z_indexes_in_overlay_layer: u8,

    /// An estimate for how many elements are expected to be in this canvas in a
    /// typical use case. This is used to pre-allocate capacity to improve slightly
    /// improve load-up times.
    ///
    /// By default this is set to `0` (no capacity will be pre-allocated).
    pub preallocate_for_this_many_elements: u32,
}

impl Default for CanvasConfig {
    fn default() -> Self {
        Self {
            canvas_size: LogicalSize::new(480.0, 240.0),
            additional_clip_areas: Vec::new(),
            num_z_indexes_in_background_layer: 2,
            num_z_indexes_in_main_layer: 4,
            num_z_indexes_in_overlay_layer: 2,
            preallocate_for_this_many_elements: 0,
        }
    }
}

pub struct Canvas {
    element_arena: IdArena<ElementEntry, Box<dyn Element>>,

    clip_areas: Vec<ClipArea>,

    mod_queue_sender: stmpsc_queue::Sender<ElementModification>,
    mod_queue_receiver: stmpsc_queue::Receiver<ElementModification>,

    animating_elements: Vec<ElementID>,
    exclusive_focus_data: Option<ExclusiveFocusData>,

    background_layer: Layer,
    main_layer: Layer,
    overlay_layer: Layer,

    canvas_needs_repaint: bool,
}

impl Canvas {
    pub fn new(config: CanvasConfig) -> Self {
        let CanvasConfig {
            canvas_size,
            mut additional_clip_areas,
            num_z_indexes_in_background_layer,
            num_z_indexes_in_main_layer,
            num_z_indexes_in_overlay_layer,
            preallocate_for_this_many_elements,
        } = config;

        assert_ne!(num_z_indexes_in_background_layer, 0);
        assert_ne!(num_z_indexes_in_main_layer, 0);
        assert_ne!(num_z_indexes_in_overlay_layer, 0);

        let mut clip_areas = Vec::with_capacity(1 + additional_clip_areas.len());
        let canvas_rect = LogicalRect::new(LogicalPoint::default(), canvas_size);
        clip_areas.push(ClipArea::new(ClipAreaConfig {
            rect: LogicalRect {
                pos: LogicalPoint::default(),
                size: canvas_size,
            },
            scroll_offset: LogicalPoint::default(),
        }));
        for config in additional_clip_areas.drain(..) {
            if !canvas_rect.contains_rect(config.rect) {
                // TODO: Log warning.
            }

            clip_areas.push(ClipArea::new(config));
        }

        let capacity = preallocate_for_this_many_elements as usize;

        let (mod_queue_sender, mod_queue_receiver) = stmpsc_queue::single_thread_mpsc_queue(
            // Give some wiggle-room since elements can be added to the queue more than once.
            capacity * 4,
        );

        Self {
            element_arena: IdArena::with_capacity(capacity),

            clip_areas,

            mod_queue_sender,
            mod_queue_receiver,

            animating_elements: Vec::with_capacity(capacity),
            exclusive_focus_data: None,

            background_layer: Layer::new(num_z_indexes_in_background_layer),
            main_layer: Layer::new(num_z_indexes_in_main_layer),
            overlay_layer: Layer::new(num_z_indexes_in_overlay_layer),

            canvas_needs_repaint: true,
        }
    }

    pub fn size(&self) -> LogicalSize {
        self.clip_areas[0].rect().size
    }

    // TODO: Custom error type.
    pub fn set_size(&mut self, size: LogicalSize) -> Result<(), ()> {
        if size.width() < 1.0 || size.height() < 1.0 {
            return Err(());
        }

        let rect_changed = self.clip_areas[0].set_rect(
            LogicalRect {
                pos: LogicalPoint { x: 0.0, y: 0.0 },
                size,
            },
            &mut self.mod_queue_sender,
        );

        if rect_changed {
            self.canvas_needs_repaint = true;
        }

        Ok(())
    }

    pub fn clip_area_rect(&self, clip_area_id: ClipAreaID) -> Option<LogicalRect> {
        self.clip_areas
            .get(usize::from(clip_area_id))
            .map(|c| *c.rect())
    }

    // TODO: Custom error type.
    pub fn set_clip_area_rect(
        &mut self,
        clip_area_id: ClipAreaID,
        new_rect: LogicalRect,
    ) -> Result<(), ()> {
        if clip_area_id == CANVAS_CLIP_AREA_ID {
            self.set_size(new_rect.size)?;
            return Ok(());
        }

        if new_rect.width() < 1.0 || new_rect.height() < 1.0 {
            return Err(());
        }

        let canvas_rect = self.clip_areas[0].rect();
        if !canvas_rect.contains_rect(new_rect) {
            // TODO: Log warning.
        }

        let clip_area = self
            .clip_areas
            .get_mut(usize::from(clip_area_id))
            .ok_or(())?;

        clip_area.set_rect(new_rect, &mut self.mod_queue_sender);

        Ok(())
    }

    pub fn clip_area_scroll_offset(&self, clip_area_id: ClipAreaID) -> Option<LogicalPoint> {
        self.clip_areas
            .get(usize::from(clip_area_id))
            .map(|c| c.scroll_offset())
    }

    // TODO: Custom error type.
    pub fn set_clip_area_scroll_offset(
        &mut self,
        clip_area_id: ClipAreaID,
        scroll_offset: LogicalPoint,
    ) -> Result<(), ()> {
        if clip_area_id == CANVAS_CLIP_AREA_ID {
            return Err(());
        }

        let clip_area = self
            .clip_areas
            .get_mut(usize::from(clip_area_id))
            .ok_or(())?;

        clip_area.set_scroll_offset(scroll_offset, &mut self.mod_queue_sender);

        Ok(())
    }

    pub fn add_element(
        &mut self,
        element_builder: ElementBuilder,
        action_sender: &mut ActionSender,
    ) -> ElementHandle {
        let ElementBuilder {
            element,
            layer_id,
            z_index,
            rect,
            manually_hidden,
            clip_area_id,
        } = element_builder;

        let flags = element.flags();

        // If the clip area ID is invalid, default to the canvas clip area.
        let clip_area_id = if usize::from(clip_area_id) >= self.clip_areas.len() {
            // TODO: Log warning.

            CANVAS_CLIP_AREA_ID
        } else {
            clip_area_id
        };

        let mut entry = ElementEntry {
            rect: LogicalRect::default(),
            pos_relative_to_clip_area: LogicalPoint::default(),
            layer_id,
            clip_area_id,
            flags,
            visible: false,
            manually_hidden,
            animating: false,
            layer_data: ElementLayerData::default(),
            index_in_animating_list: 0,
            index_in_clip_area_list: 0,
        };

        entry.set_layout(rect.pos, rect.size, &self.clip_areas);
        entry.check_and_update_visibility(&self.clip_areas);

        if entry.visible && entry.flags.contains(ElementFlags::PAINTS) {
            self.canvas_needs_repaint = true;
        }

        let element_id = ElementID(self.element_arena.insert(entry, element));

        let clip_area = &mut self.clip_areas[usize::from(clip_area_id)];

        let layer = match layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        let (element_entry, element) = self.element_arena.get_mut(element_id.0).unwrap();

        clip_area.add_element(element_entry, element_id);
        layer.add_element(element_entry, element_id, z_index);

        send_event_to_element(
            Event::Init,
            element_entry,
            element,
            element_id,
            &self.exclusive_focus_data,
            &mut self.mod_queue_sender,
            action_sender,
        );

        self::element::new_element_handle(
            element_id,
            self.mod_queue_sender.clone(),
            rect,
            z_index,
            manually_hidden,
        )
    }

    pub fn handle_event(
        &mut self,
        event: &CanvasEvent,
        action_sender: &mut ActionSender,
    ) -> EventCaptureStatus {
        match event {
            CanvasEvent::Animation { delta_seconds } => {
                self.handle_animation_event(*delta_seconds, action_sender);

                // Capture status is not relavant for this event.
                EventCaptureStatus::NotCaptured
            }
            CanvasEvent::Pointer(pointer_event) => {
                self.handle_pointer_event(pointer_event, action_sender)
            }
            CanvasEvent::Keyboard(keyboard_event) => {
                self.handle_keyboard_event(keyboard_event, action_sender)
            }
            CanvasEvent::TextComposition(text_composition_event) => {
                self.handle_text_composition_event(text_composition_event, action_sender)
            }
            CanvasEvent::WindowHidden => {
                // TODO
                EventCaptureStatus::NotCaptured
            }
            CanvasEvent::WindowShown => {
                // TODO
                EventCaptureStatus::NotCaptured
            }
            CanvasEvent::WindowFocused => {
                // TODO
                EventCaptureStatus::NotCaptured
            }
            CanvasEvent::WindowUnfocused => {
                // TODO
                EventCaptureStatus::NotCaptured
            }
        }
    }

    fn handle_animation_event(&mut self, delta_seconds: f64, action_sender: &mut ActionSender) {
        for element_id in self.animating_elements.iter() {
            let (element_entry, element) = self.element_arena.get_mut(element_id.0).unwrap();

            let _ = send_event_to_element(
                Event::Animation { delta_seconds },
                element_entry,
                element,
                *element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }
    }

    fn handle_pointer_event(
        &mut self,
        event: &PointerEvent,
        action_sender: &mut ActionSender,
    ) -> EventCaptureStatus {
        // Focused elements get first priority.
        if let Some(focused_data) = &self.exclusive_focus_data {
            if focused_data.listens_to_pointer_inside_bounds
                || focused_data.listens_to_pointer_outside_bounds
            {
                let (element_entry, element) = self
                    .element_arena
                    .get_mut(focused_data.element_id.0)
                    .unwrap();

                let send_event = if focused_data.listens_to_pointer_outside_bounds {
                    true
                } else {
                    element_entry.rect.contains_point(event.position)
                };

                if send_event {
                    let capture_satus = send_event_to_element(
                        Event::Pointer(event.clone()),
                        element_entry,
                        element,
                        focused_data.element_id,
                        &self.exclusive_focus_data,
                        &mut self.mod_queue_sender,
                        action_sender,
                    );

                    if let EventCaptureStatus::Captured = capture_satus {
                        return EventCaptureStatus::Captured;
                    }
                }
            }
        }

        if let EventCaptureStatus::Captured = self.overlay_layer.handle_pointer_event(
            &event,
            &mut self.element_arena,
            &self.exclusive_focus_data,
            &mut self.mod_queue_sender,
            action_sender,
        ) {
            return EventCaptureStatus::Captured;
        }

        // TODO: Handle "clicking outside overlay elements to close them".

        if let EventCaptureStatus::Captured = self.main_layer.handle_pointer_event(
            &event,
            &mut self.element_arena,
            &self.exclusive_focus_data,
            &mut self.mod_queue_sender,
            action_sender,
        ) {
            return EventCaptureStatus::Captured;
        }

        self.background_layer.handle_pointer_event(
            &event,
            &mut self.element_arena,
            &self.exclusive_focus_data,
            &mut self.mod_queue_sender,
            action_sender,
        )
    }

    fn handle_keyboard_event(
        &mut self,
        event: &KeyboardEvent,
        action_sender: &mut ActionSender,
    ) -> EventCaptureStatus {
        if let Some(focused_data) = &self.exclusive_focus_data {
            if focused_data.listens_to_keys {
                let (element_entry, element) = self
                    .element_arena
                    .get_mut(focused_data.element_id.0)
                    .unwrap();

                let capture_satus = send_event_to_element(
                    Event::Keyboard(event.clone()),
                    element_entry,
                    element,
                    focused_data.element_id,
                    &self.exclusive_focus_data,
                    &mut self.mod_queue_sender,
                    action_sender,
                );

                if let EventCaptureStatus::Captured = capture_satus {
                    return EventCaptureStatus::Captured;
                }
            }
        }

        EventCaptureStatus::NotCaptured
    }

    fn handle_text_composition_event(
        &mut self,
        event: &CompositionEvent,
        action_sender: &mut ActionSender,
    ) -> EventCaptureStatus {
        if let Some(focused_data) = &self.exclusive_focus_data {
            if focused_data.listens_to_text_composition {
                let (element_entry, element) = self
                    .element_arena
                    .get_mut(focused_data.element_id.0)
                    .unwrap();

                let capture_satus = send_event_to_element(
                    Event::TextComposition(event.clone()),
                    element_entry,
                    element,
                    focused_data.element_id,
                    &self.exclusive_focus_data,
                    &mut self.mod_queue_sender,
                    action_sender,
                );

                if let EventCaptureStatus::Captured = capture_satus {
                    return EventCaptureStatus::Captured;
                }
            }
        }

        EventCaptureStatus::NotCaptured
    }

    pub fn process_updates(&mut self, action_sender: &mut ActionSender) {
        while let Some(modification) = self.mod_queue_receiver.try_recv() {
            match modification.type_ {
                ElementModificationType::MarkDirty => {
                    self.mark_element_dirty(modification.element_id);
                }
                ElementModificationType::RectChanged(new_rect) => {
                    self.update_element_rect(modification.element_id, new_rect, action_sender);
                }
                ElementModificationType::ClipAreaChanged => {
                    self.handle_clip_area_changed_for_element(
                        modification.element_id,
                        action_sender,
                    );
                }
                ElementModificationType::ZIndexChanged(new_z_index) => {
                    self.update_element_z_index(
                        modification.element_id,
                        new_z_index,
                        action_sender,
                    );
                }
                ElementModificationType::ExplicitlyHiddenChanged(manually_hidden) => {
                    self.update_element_manually_hidden(
                        modification.element_id,
                        manually_hidden,
                        action_sender,
                    );
                }
                ElementModificationType::SetAnimating(animating) => {
                    self.set_element_animating(modification.element_id, animating);
                }
                ElementModificationType::StealExclusiveFocus => {
                    self.element_steal_exclusive_focus(modification.element_id, action_sender);
                }
                ElementModificationType::ReleaseExclusiveFocus => {
                    self.element_release_exclusive_focus(modification.element_id, action_sender);
                }
                ElementModificationType::HandleDropped => {
                    self.drop_element(modification.element_id, action_sender);
                }
            }
        }
    }

    pub fn canvas_needs_repaint(&self) -> bool {
        self.canvas_needs_repaint
    }

    fn mark_element_dirty(&mut self, element_id: ElementID) {
        let Some(element_entry) = self.element_arena.get_stack_data(element_id.0) else {
            // Element has been dropped. Do nothing and return.
            return;
        };

        if !element_entry.flags.contains(ElementFlags::PAINTS) || !element_entry.visible {
            return;
        }

        let layer = match element_entry.layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        layer.mark_element_dirty(element_entry);

        self.canvas_needs_repaint = true;
    }

    fn update_element_rect(
        &mut self,
        element_id: ElementID,
        new_rect: LogicalRect,
        action_sender: &mut ActionSender,
    ) {
        let Some((element_entry, element)) = self.element_arena.get_mut(element_id.0) else {
            // Element has been dropped. Do nothing and return.
            return;
        };

        let pos_changed = element_entry.pos_relative_to_clip_area != new_rect.pos;
        let size_changed = element_entry.rect.size != new_rect.size;

        if !(pos_changed || size_changed) {
            return;
        }

        element_entry.set_layout(new_rect.pos, new_rect.size, &self.clip_areas);

        let visibility_changed = element_entry.check_and_update_visibility(&self.clip_areas);

        if size_changed
            && element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_SIZE_CHANGE)
        {
            send_event_to_element(
                Event::SizeChanged,
                element_entry,
                element,
                element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }

        if visibility_changed
            && element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_VISIBILITY_CHANGE)
        {
            let event = if element_entry.visible {
                Event::Shown
            } else {
                Event::Hidden
            };

            send_event_to_element(
                event,
                element_entry,
                element,
                element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }

        let layer = match element_entry.layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        layer.sync_element_cache(element_entry);

        if element_entry.visible || visibility_changed {
            self.canvas_needs_repaint = true;
        }
    }

    fn handle_clip_area_changed_for_element(
        &mut self,
        element_id: ElementID,
        action_sender: &mut ActionSender,
    ) {
        let Some((element_entry, element)) = self.element_arena.get_mut(element_id.0) else {
            // Element has been dropped. Do nothing and return.
            return;
        };

        element_entry.update_layout(&self.clip_areas);

        let visibility_changed = element_entry.check_and_update_visibility(&self.clip_areas);

        let layer = match element_entry.layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        layer.sync_element_cache(element_entry);

        if visibility_changed
            && element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_VISIBILITY_CHANGE)
        {
            let event = if element_entry.visible {
                Event::Shown
            } else {
                Event::Hidden
            };

            send_event_to_element(
                event,
                element_entry,
                element,
                element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }

        if element_entry.visible || visibility_changed {
            self.canvas_needs_repaint = true;
        }
    }

    fn update_element_z_index(
        &mut self,
        element_id: ElementID,
        new_z_index: ZIndex,
        action_sender: &mut ActionSender,
    ) {
        let layer_id = {
            let Some(element_entry) = self.element_arena.get_stack_data(element_id.0) else {
                // Element has been dropped. Do nothing and return.
                return;
            };

            element_entry.layer_id
        };

        let layer = match layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        let z_index_changed =
            layer.sync_element_z_index(element_id, new_z_index, &mut self.element_arena);

        if !z_index_changed {
            return;
        }

        let Some((element_entry, element)) = self.element_arena.get_mut(element_id.0) else {
            return;
        };

        if element_entry
            .flags
            .contains(ElementFlags::LISTENS_TO_Z_INDEX_CHANGE)
        {
            send_event_to_element(
                Event::ZIndexChanged,
                element_entry,
                element,
                element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }

        // Detecting if a z index change requires a repaint or not would be very tricky,
        // so just repaint regardless if the element is visible.
        if element_entry.visible {
            self.canvas_needs_repaint = true;
        }
    }

    fn update_element_manually_hidden(
        &mut self,
        element_id: ElementID,
        manually_hidden: bool,
        action_sender: &mut ActionSender,
    ) {
        let Some((element_entry, element)) = self.element_arena.get_mut(element_id.0) else {
            // Element has been dropped. Do nothing and return.
            return;
        };

        if element_entry.manually_hidden == manually_hidden {
            return;
        }

        element_entry.manually_hidden = manually_hidden;

        let visibility_changed = element_entry.check_and_update_visibility(&self.clip_areas);

        if !visibility_changed {
            return;
        }

        let layer = match element_entry.layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        layer.sync_element_cache(element_entry);

        if element_entry
            .flags
            .contains(ElementFlags::LISTENS_TO_VISIBILITY_CHANGE)
        {
            let event = if element_entry.visible {
                Event::Shown
            } else {
                Event::Hidden
            };

            send_event_to_element(
                event,
                element_entry,
                element,
                element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }

        self.canvas_needs_repaint = true;
    }

    fn set_element_animating(&mut self, element_id: ElementID, animating: bool) {
        let Some(element_entry) = self.element_arena.get_stack_data_mut(element_id.0) else {
            // Element has been dropped. Do nothing and return.
            return;
        };

        if element_entry.animating == animating {
            return;
        }

        element_entry.animating = animating;

        if animating {
            element_entry.index_in_animating_list = self.animating_elements.len() as u32;
            self.animating_elements.push(element_id);
        } else {
            let _ = self
                .animating_elements
                .swap_remove(element_entry.index_in_animating_list as usize);

            // Update the index on the element that was swapped.
            if let Some(swapped_element_id) = self
                .animating_elements
                .get(element_entry.index_in_animating_list as usize)
                .copied()
            {
                self.element_arena
                    .get_stack_data_mut(swapped_element_id.0)
                    .as_mut()
                    .unwrap()
                    .index_in_animating_list = element_entry.index_in_animating_list;
            }
        }
    }

    fn element_steal_exclusive_focus(
        &mut self,
        element_id: ElementID,
        action_sender: &mut ActionSender,
    ) {
        if self.element_arena.get_stack_data(element_id.0).is_none() {
            // Element has been dropped. Do nothing and return.
            return;
        }

        // Release focus from the previously focused element.
        let just_stole_focus = if let Some(exclusive_focus_data) = &self.exclusive_focus_data {
            if exclusive_focus_data.element_id != element_id {
                self.element_release_exclusive_focus(
                    exclusive_focus_data.element_id,
                    action_sender,
                );
                true
            } else {
                false
            }
        } else {
            true
        };

        let (element_entry, element) = self.element_arena.get_mut(element_id.0).unwrap();

        self.exclusive_focus_data = Some(ExclusiveFocusData {
            element_id,
            listens_to_pointer_inside_bounds: element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_POINTER_INSIDE_BOUNDS),
            listens_to_pointer_outside_bounds: element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_POINTER_OUTSIDE_BOUNDS_WHEN_FOCUSED),
            listens_to_text_composition: element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_TEXT_COMPOSITION_WHEN_FOCUSED),
            listens_to_keys: element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_KEYS_WHEN_FOCUSED),
        });

        if just_stole_focus
            && element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_FOCUS_CHANGE)
        {
            send_event_to_element(
                Event::GotExclusiveFocus,
                element_entry,
                element,
                element_id,
                &self.exclusive_focus_data,
                &mut self.mod_queue_sender,
                action_sender,
            );
        }
    }

    fn element_release_exclusive_focus(
        &mut self,
        element_id: ElementID,
        action_sender: &mut ActionSender,
    ) {
        let Some((element_entry, element)) = self.element_arena.get_mut(element_id.0) else {
            // Element has been dropped. Do nothing and return.
            return;
        };

        let has_focus = if let Some(exclusive_focus_data) = &self.exclusive_focus_data {
            exclusive_focus_data.element_id == element_id
        } else {
            false
        };

        if has_focus {
            self.exclusive_focus_data = None;

            if element_entry
                .flags
                .contains(ElementFlags::LISTENS_TO_FOCUS_CHANGE)
            {
                self.exclusive_focus_data = None;

                send_event_to_element(
                    Event::ExclusiveFocusReleased,
                    element_entry,
                    element,
                    element_id,
                    &self.exclusive_focus_data,
                    &mut self.mod_queue_sender,
                    action_sender,
                );
            }
        }
    }

    fn drop_element(&mut self, element_id: ElementID, action_sender: &mut ActionSender) {
        if let Some(exclusive_focus_data) = &self.exclusive_focus_data {
            if exclusive_focus_data.element_id == element_id {
                self.element_release_exclusive_focus(element_id, action_sender);
            }
        }

        let Some((element_entry, mut element)) = self.element_arena.remove(element_id.0) else {
            // Element has already been dropped. Do nothing and return.
            return;
        };

        if element_entry
            .flags
            .contains(ElementFlags::LISTENS_TO_ON_DROPPED)
        {
            element.on_dropped(action_sender);
        }

        if element_entry.animating {
            let _ = self
                .animating_elements
                .swap_remove(element_entry.index_in_animating_list as usize);

            // Update the index on the element that was swapped.
            if let Some(swapped_element_id) = self
                .animating_elements
                .get(element_entry.index_in_animating_list as usize)
                .copied()
            {
                self.element_arena
                    .get_stack_data_mut(swapped_element_id.0)
                    .as_mut()
                    .unwrap()
                    .index_in_animating_list = element_entry.index_in_animating_list;
            }
        }

        let clip_area = &mut self.clip_areas[usize::from(element_entry.clip_area_id)];

        clip_area.remove_element(&element_entry, &mut self.element_arena);

        let layer = match element_entry.layer_id {
            LayerID::Background => &mut self.background_layer,
            LayerID::Main => &mut self.main_layer,
            LayerID::Overlay => &mut self.overlay_layer,
        };

        layer.remove_element(&element_entry, &mut self.element_arena);

        if element_entry.visible {
            self.canvas_needs_repaint = true;
        }
    }

    pub fn render(&mut self) {
        if !self.canvas_needs_repaint {
            return;
        }
        self.canvas_needs_repaint = false;

        self.background_layer.render();
        self.main_layer.render();
        self.overlay_layer.render();

        // TODO
    }
}

// Ideally the size of this struct should be as small as possible to
// maximize cache locality when accessing entries at random from the
// arena.
//
// Since an application is probably never going to have more than 4
// billion elements anyway, I've opted to use u32 for indexes.
#[derive(Clone)]
struct ElementEntry {
    rect: LogicalRect,
    pos_relative_to_clip_area: LogicalPoint,

    layer_id: LayerID,
    clip_area_id: ClipAreaID,

    flags: ElementFlags,
    visible: bool,
    manually_hidden: bool,
    animating: bool,

    layer_data: ElementLayerData,

    index_in_animating_list: u32,
    index_in_clip_area_list: u32,
}

impl ElementEntry {
    fn set_layout(
        &mut self,
        pos_relative_to_clip_area: LogicalPoint,
        size: LogicalSize,
        clip_areas: &[ClipArea],
    ) {
        self.pos_relative_to_clip_area = pos_relative_to_clip_area;
        self.rect.size = size;

        self.update_layout(clip_areas)
    }

    #[inline]
    fn update_layout(&mut self, clip_areas: &[ClipArea]) {
        // SAFETY:
        // - The `Canvas` made sure that the clip area ID is valid when this
        // element entry was created. This index and the number of clip areas
        // do not change.
        // - The fact that element modifications must go through a certain
        // queue ensures that only elements belonging to this canvas instance
        // are processed by the canvas instance. So we know that `clip_areas`
        // comes from the canvas instance that this element entry belongs to.
        let clip_area = unsafe { clip_areas.get_unchecked(usize::from(self.clip_area_id)) };

        self.rect.pos =
            clip_area.pos() + self.pos_relative_to_clip_area + clip_area.scroll_offset();
    }

    /// Calculates whether or not this element is currently visible.
    ///
    /// Returns `true` if the visibility changed, `false` otherwise.
    fn check_and_update_visibility(&mut self, clip_areas: &[ClipArea]) -> bool {
        let old_visible = self.visible;

        self.visible = if self.manually_hidden || self.rect.size.is_zero() {
            false
        } else {
            // SAFETY:
            // - The `Canvas` made sure that the clip area ID is valid when this
            // element entry was created. This index and the number of clip areas
            // do not change.
            // - The fact that element modifications must go through a certain
            // queue ensures that only elements belonging to this canvas instance
            // are processed by the canvas instance. So we know that `clip_areas`
            // comes from the canvas instance that this element entry belongs to.
            unsafe {
                clip_areas
                    .get_unchecked(usize::from(self.clip_area_id))
                    .rect()
                    .overlaps_with_rect(self.rect)
            }
        };

        self.visible != old_visible
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LayerID {
    Background,
    Main,
    Overlay,
}

struct ExclusiveFocusData {
    element_id: ElementID,
    listens_to_pointer_inside_bounds: bool,
    listens_to_pointer_outside_bounds: bool,
    listens_to_text_composition: bool,
    listens_to_keys: bool,
}

fn send_event_to_element(
    event: Event,
    element_entry: &mut ElementEntry,
    element: &mut Box<dyn Element>,
    element_id: ElementID,
    exclusive_focus_data: &Option<ExclusiveFocusData>,
    mod_queue_sender: &mut stmpsc_queue::Sender<ElementModification>,
    action_sender: &mut ActionSender,
) -> EventCaptureStatus {
    let has_exclusive_focus = exclusive_focus_data
        .as_ref()
        .map(|d| d.element_id == element_id)
        .unwrap_or(false);

    let mut cx = ElementContext::new(
        element_entry.rect,
        element_entry.layer_data.z_index(),
        element_entry.manually_hidden,
        element_entry.animating,
        element_entry.visible,
        has_exclusive_focus,
    );

    let capture_status = element.on_event(event, &mut cx, action_sender);

    if cx.repaint_requested() {
        mod_queue_sender.send_to_front(ElementModification {
            element_id,
            type_: ElementModificationType::MarkDirty,
        });
    }

    if cx.is_animating() != element_entry.animating {
        mod_queue_sender.send_to_front(ElementModification {
            element_id,
            type_: ElementModificationType::SetAnimating(cx.is_animating()),
        });
    }

    if cx.has_exclusive_focus() != has_exclusive_focus {
        let type_ = if cx.has_exclusive_focus() {
            ElementModificationType::StealExclusiveFocus
        } else {
            ElementModificationType::ReleaseExclusiveFocus
        };

        mod_queue_sender.send_to_front(ElementModification { element_id, type_ });
    }

    capture_status
}
