// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

use std::ops::{Add, AddAssign, Sub, SubAssign};

pub type ZIndex = u8;

pub type ScaleFactor = f32;

/// A size in logical coordinates (points)
#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct LogicalSize {
    width: f32,
    height: f32,
}

impl LogicalSize {
    /// Create a new size in logical coordinates (points).
    ///
    /// If any of the given values are less than zero, then they will
    /// be set to zero.
    pub fn new(width: f32, height: f32) -> Self {
        Self {
            width: width.max(0.0),
            height: height.max(0.0),
        }
    }

    /// Convert to physical size (pixels)
    pub fn to_physical(&self, scale_factor: ScaleFactor) -> PhysicalSize {
        PhysicalSize {
            width: (self.width * scale_factor).round() as u32,
            height: (self.height * scale_factor).round() as u32,
        }
    }

    pub const fn width(&self) -> f32 {
        self.width
    }

    pub const fn height(&self) -> f32 {
        self.height
    }

    /// Set the width.
    ///
    /// If the given value is less than zero, then the width will
    /// be set to zero.
    pub fn set_width(&mut self, width: f32) {
        self.width = width.max(0.0);
    }

    /// Set the height.
    ///
    /// If the given value is less than zero, then the height will
    /// be set to zero.
    pub fn set_height(&mut self, height: f32) {
        self.height = height.max(0.0);
    }

    pub fn min(&self, other: Self) -> Self {
        Self {
            width: self.width.min(other.width),
            height: self.height.min(other.height),
        }
    }

    pub fn max(&self, other: Self) -> Self {
        Self {
            width: self.width.max(other.width),
            height: self.height.max(other.height),
        }
    }

    pub fn approx_eq(&self, other: Self) -> bool {
        ((self.width - other.width).abs() <= f32::EPSILON)
            && ((self.height - other.height).abs() <= f32::EPSILON)
    }

    pub fn is_zero(&self) -> bool {
        self.width == 0.0 && self.height == 0.0
    }
}

/// A size in physical coordinates (pixels)
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
pub struct PhysicalSize {
    pub width: u32,
    pub height: u32,
}

impl PhysicalSize {
    /// Create a new size in physical coordinates (pixels)
    pub const fn new(width: u32, height: u32) -> Self {
        Self { width, height }
    }

    /// Convert to logical size (points)
    pub fn to_logical(&self, scale_factor: ScaleFactor) -> LogicalSize {
        LogicalSize {
            width: self.width as f32 / scale_factor,
            height: self.height as f32 / scale_factor,
        }
    }

    /// Convert to logical size (points) using the reciprocal of the scale factor
    pub fn to_logical_from_scale_recip(&self, scale_recip: f32) -> LogicalSize {
        LogicalSize {
            width: self.width as f32 * scale_recip,
            height: self.height as f32 * scale_recip,
        }
    }
}

/// A point in logical coordinates (points)
#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct LogicalPoint {
    pub x: f32,
    pub y: f32,
}

impl LogicalPoint {
    /// Create a new point in logical coordinates (points)
    pub const fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    /// Convert to physical coordinates (pixels)
    #[inline]
    pub fn to_physical(&self, scale_factor: ScaleFactor) -> PhysicalPoint {
        PhysicalPoint {
            x: (self.x * scale_factor).round() as i32,
            y: (self.y * scale_factor).round() as i32,
        }
    }

    #[inline]
    pub fn approx_eq(&self, other: Self) -> bool {
        ((self.x - other.x).abs() <= f32::EPSILON) && ((self.y - other.y).abs() <= f32::EPSILON)
    }
}

impl Add<LogicalPoint> for LogicalPoint {
    type Output = LogicalPoint;
    fn add(self, rhs: LogicalPoint) -> Self::Output {
        LogicalPoint {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub<LogicalPoint> for LogicalPoint {
    type Output = LogicalPoint;
    fn sub(self, rhs: LogicalPoint) -> Self::Output {
        LogicalPoint {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl AddAssign for LogicalPoint {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs
    }
}

impl SubAssign for LogicalPoint {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs
    }
}

/// A point in physical coordinates (pixels)
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
pub struct PhysicalPoint {
    pub x: i32,
    pub y: i32,
}

impl PhysicalPoint {
    /// Create a new point in physical coordinates (pixels)
    pub const fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    /// Convert to logical coordinates (points)
    pub fn to_logical(&self, scale_factor: ScaleFactor) -> LogicalPoint {
        LogicalPoint {
            x: self.x as f32 / scale_factor,
            y: self.y as f32 / scale_factor,
        }
    }

    /// Convert to logical coordinates (points) using the reciprocal of the scale factor
    pub fn to_logical_from_scale_recip(&self, scale_recip: f32) -> LogicalPoint {
        LogicalPoint {
            x: self.x as f32 * scale_recip,
            y: self.y as f32 * scale_recip,
        }
    }
}

/// A rectangle in logical coordinates (points)
#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct LogicalRect {
    pub pos: LogicalPoint,
    pub size: LogicalSize,
}

impl LogicalRect {
    pub const fn new(pos: LogicalPoint, size: LogicalSize) -> Self {
        Self { pos, size }
    }

    pub fn from_two_points(pos1: LogicalPoint, pos2: LogicalPoint) -> Self {
        Self {
            pos: LogicalPoint {
                x: pos1.x.min(pos2.x),
                y: pos1.y.min(pos2.y),
            },
            size: LogicalSize {
                width: (pos2.x - pos1.x).abs(),
                height: (pos2.y - pos1.y).abs(),
            },
        }
    }

    pub const fn x(&self) -> f32 {
        self.pos.x
    }

    pub const fn y(&self) -> f32 {
        self.pos.y
    }

    pub const fn width(&self) -> f32 {
        self.size.width
    }

    pub const fn height(&self) -> f32 {
        self.size.height
    }

    #[inline]
    pub fn x2(&self) -> f32 {
        self.pos.x + self.size.width
    }

    #[inline]
    pub fn y2(&self) -> f32 {
        self.pos.y + self.size.height
    }

    #[inline]
    pub fn pos_br(&self) -> LogicalPoint {
        LogicalPoint::new(self.x2(), self.y2())
    }

    #[inline]
    pub fn center_x(&self) -> f32 {
        self.pos.x + (self.size.width / 2.0)
    }

    #[inline]
    pub fn center_y(&self) -> f32 {
        self.pos.y + (self.size.height / 2.0)
    }

    pub fn center_pos(&self) -> LogicalPoint {
        LogicalPoint::new(self.center_x(), self.center_y())
    }

    pub fn contains_point(&self, point: LogicalPoint) -> bool {
        let pos_br = self.pos_br();

        point.x >= self.pos.x && point.y >= self.pos.y && point.x <= pos_br.x && point.y <= pos_br.y
    }

    pub fn overlaps_with_rect(&self, other: LogicalRect) -> bool {
        let self_pos_br = self.pos_br();
        let other_pos_br = other.pos_br();

        self_pos_br.x >= other.pos.x
            && other_pos_br.x >= self.pos.x
            && self_pos_br.y >= other.pos.y
            && other_pos_br.y >= self.pos.y
    }

    pub fn contains_rect(&self, other: LogicalRect) -> bool {
        let self_pos_br = self.pos_br();
        let other_pos_br = other.pos_br();

        other.pos.x >= self.pos.x
            && other_pos_br.x <= self_pos_br.x
            && other.pos.y >= self.pos.y
            && other_pos_br.y <= self_pos_br.y
    }

    pub fn approx_eq(&self, other: LogicalRect) -> bool {
        self.pos.approx_eq(other.pos) && self.size.approx_eq(other.size)
    }

    /// Convert to physical coordinates (pixels)
    pub fn to_physical(&self, scale_factor: ScaleFactor) -> PhysicalRect {
        PhysicalRect {
            pos: self.pos.to_physical(scale_factor),
            size: self.size.to_physical(scale_factor),
        }
    }
}

/// A rectangle in physical coordinates (pixels)
#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct PhysicalRect {
    pub pos: PhysicalPoint,
    pub size: PhysicalSize,
}

impl PhysicalRect {
    pub const fn new(pos: PhysicalPoint, size: PhysicalSize) -> Self {
        Self { pos, size }
    }

    pub fn x2(&self) -> i32 {
        self.pos.x + self.size.width as i32
    }

    pub fn y2(&self) -> i32 {
        self.pos.y + self.size.height as i32
    }

    pub fn pos_br(&self) -> PhysicalPoint {
        PhysicalPoint {
            x: self.x2(),
            y: self.y2(),
        }
    }

    /// Convert to logical coordinates (points)
    pub fn to_logical(&self, scale_factor: ScaleFactor) -> LogicalRect {
        LogicalRect::new(
            self.pos.to_logical(scale_factor),
            self.size.to_logical(scale_factor),
        )
    }

    /// Convert to logical coordinates (points) using the reciprocal of the scale factor
    pub fn to_logical_from_scale_recip(&self, scale_recip: f32) -> LogicalRect {
        LogicalRect::new(
            self.pos.to_logical_from_scale_recip(scale_recip),
            self.size.to_logical_from_scale_recip(scale_recip),
        )
    }
}
