// ---------------------------------------------------------------------------------
//  ╭───────────────────╮
//  │     ___/__%%%%%%  │    Goldenrod GUI Library
//  │   -¯    \         │
//  │  /                │    https://codeberg.org/BillyDM/Goldenrod
//  ╰───────────────────╯
//
// MIT License Copyright (c) 2024 Billy Messenger
// https://codeberg.org/BillyDM/Goldenrod/src/branch/main/LICENSE
//
// ---------------------------------------------------------------------------------

use std::any::Any;
use std::sync::mpsc;

pub fn action_channel() -> (ActionSender, ActionReceiver) {
    let (sender, receiver) = mpsc::channel();
    (ActionSender { sender }, ActionReceiver { receiver })
}

#[derive(Clone)]
pub struct ActionSender {
    pub sender: mpsc::Sender<Box<dyn Any>>,
}

impl ActionSender {
    pub fn send<A: Any>(&mut self, action: A) -> Result<(), mpsc::SendError<Box<dyn Any>>> {
        self.sender.send(Box::new(action))
    }
}

pub struct ActionReceiver {
    pub receiver: mpsc::Receiver<Box<dyn Any>>,
}

impl ActionReceiver {
    pub fn try_recv(&mut self) -> Result<Box<dyn Any>, mpsc::TryRecvError> {
        self.receiver.try_recv()
    }

    pub fn try_iter(&mut self) -> mpsc::TryIter<Box<dyn Any>> {
        self.receiver.try_iter()
    }
}
